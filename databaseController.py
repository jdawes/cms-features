from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, text
from ormClasses import *
from errorHandling import errorHandling

# except statements have been changed to throw errors with the error handling class
# this should work, but some errors may result

# use this as a controller for the database
# command interpreter can use this so we don't have to put logic in the command interpreter
class dbControl():
	# database path has to be absolute for the whole program to work in any directory
	database = "sqlite:////data/services/featureControl/tracking.db"
	engine = None
	session = None
	errors = None
	def __init__(self):
		self.engine = create_engine(self.database)
		self.session = sessionmaker(bind=self.engine)()
		self.errors = errorHandling(fileName=None)
		# we now have a connection to the database, and we can perform queries

	def addAndCommit(self, record):
		try:
			self.session.add(record)
			self.session.commit();
			return True
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def getFilesForProject(self, projectID):
		try:
			return self.session.query(File).filter(File.projectID == projectID).all()
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def getFilesForSelectedProject(self):
		# assume a project is selected - since in the function that calls this (queryCommand in commandInterpreter.py),
		# the project check is done before calling this
		try:
			# get selected project
			project = self.session.query(Project).filter(Project.current == 1).first()
			files = self.getFilesForProject(project.id)
			return files
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def getFileFromID(self, fileID):
		try:
			fileObject = self.session.query(File).filter(File.id == fileID).first()
			return fileObject
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def deleteFile(self, absFileName):
		try:
			# absFileName will be an absolute directory (preformatted in commandInterpreter.py)
			# we should use project id of selected project as well to
			# this allows file sharing between projects
			selectedProject = self.getSelectedProject()
			fileObject = self.session.query(File).filter(File.fileName == absFileName, File.projectID == selectedProject.id).first()
			if fileObject == None:
				self.errors.throwApplicationError("Cannot remove file that isn't in the tracking database.\n"\
											+ "Note: if you have manually removed data without imposing the foreign key restrictions, "\
											+ "you can fix this by querying the tracking database manually.")
			self.session.delete(fileObject)
			self.session.commit()
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def isFeatureFinished(self, featureID):
		try:
			# looks at all code fragments belonging to this feature
			# if either has finished=0, then the feature is not finished
			allFragments = self.session.query(CodeFragment).filter(CodeFragment.featureID == featureID).all()
			fragments = self.session.query(CodeFragment).filter(CodeFragment.featureID == featureID, CodeFragment.finished == 0).all()
			if len(allFragments) == 0:
				return False
			else:
				return len(fragments) == 0
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def isFeatureTested(self, featureID):
		try:
			# looks at all code fragments belonging to this feature
			# if either has finished=0, then the feature is not finished
			allFragments = self.session.query(CodeFragment).filter(CodeFragment.featureID == featureID).all()
			fragments = self.session.query(CodeFragment).filter(CodeFragment.featureID == featureID, CodeFragment.tested == 0).all()
			if len(allFragments) == 0:
				return False
			else:
				return len(fragments) == 0
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def featureInSelectedProject(self, featureID):
		try:
			# get information about feature, then check that the feature's project
			# is the same as the selected project
			selectedProject = self.getSelectedProject()
			feature = self.session.query(Feature).filter(Feature.id == featureID).first()
			if feature != None:
				return int(feature.projectID) == int(selectedProject.id)
			else:
				raise Exception("No feature can be found with this ID.")
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e


	# at the moment, this does not support querying comments
	# to query strings maybe detecting non-numerical characters and wrapping in quotes will suffice
	def getFragmentsWithConfig(self, configDictionary):
		try:
			# convert config dictionary to list of lists
			configList = map(lambda l : list(l), configDictionary.items())
			# now go through each key in configDictionary and filter
			# will have to construct sql string
			queryString = ""
			for n in range(0, len(configList)):
				if n != len(configList)-1:
					suffix = " and "
				else:
					suffix = ""
				queryString += "codeFragment.%s = %s%s" % (configList[n][0], configList[n][1], suffix)
			query = filter(
				lambda fragment : self.featureInSelectedProject(fragment.featureID),
				self.session.query(CodeFragment).filter(text(queryString)).all()
				)
			return query
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def getSelectedProject(self):
		try:
			# query for project.current == 1
			project = self.session.query(Project).filter(Project.current == 1)
			if project.count() == 1:
				return project.first()
			else:
				# throw an application error - doing it here means writing less code
				self.errors.throwApplicationError("No single project was selected.  Run the 'select project' command.")
				#return project.count()
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def getFragmentsForFeature(self, featureName):
		try:
			selectedProject = self.getSelectedProject()
			if type(selectedProject) == Project:
				feature = self.session.query(Feature).filter(Feature.projectID == selectedProject.id, Feature.name == featureName).first()
				if feature != None:
					fragments = self.session.query(CodeFragment).filter(CodeFragment.featureID == feature.id).all()
					if fragments != None:
						return fragments
					else:
						# this isn't exactly an error, but it makes handling exceptional cases in commandInterpreter.py
						# a bit easier
						raise Exception("No fragments were found for this feature.")
				else:
					raise Exception("No feature exists with this name, in this project.")
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def getFragmentsInFile(self, fileName):
		try:
			selectedProject = self.getSelectedProject()
			if type(selectedProject) == Project:
				# get file id
				fileObject = self.session.query(File).filter(File.fileName == fileName, File.projectID == selectedProject.id).first()
				if fileObject == None:
					self.errors.throwApplicationError("It looks like a file was removed at some point during this command's execution.", exitOnReport=True)
				fragments = self.session.query(CodeFragment).filter(CodeFragment.fileID == fileObject.id).all()
				return fragments
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e			

	def getContributingFragments(self, featureID):
		try:
			fragments = self.session.query(CodeFragment).filter(CodeFragment.featureID == featureID).all()
			return fragments
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def getTotalFilesFromFragments(self, fragments):
		# map list of fragments to list of fragment file IDs,
		# then convert to a set to get unique elements,
		# then convert back to list to count
		uniqueFiles = list(set(map(lambda fragment : fragment.fileID, fragments)))
		return len(uniqueFiles)

	def getFeaturesFromProject(self):
		# get list of features from selected project
		try:
			selectedProject = self.getSelectedProject()
			if type(selectedProject) == Project:
				features = self.session.query(Feature).filter(Feature.projectID == selectedProject.id).order_by(Feature.name).all()
				return features
			else:
				raise Exception("A single project is not selected.  Exactly 1 must be selected to perform any queries.")
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def getFeatureFromID(self, id):
		try:
			feature = self.session.query(Feature).filter(Feature.id == id).first()
			if feature != None:
				return feature
			else:
				return Exception("No feature exists with this ID.")
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def getFeatureFromName(self, featureName):
		try:
			selectedProject = self.getSelectedProject()
			feature = self.session.query(Feature).filter(Feature.name == featureName, Feature.projectID == selectedProject.id).first()
			if feature != None:
				return feature
			else:
				raise Exception("No feature exists with this name, in this project.")
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e			

	def getAllProjects(self):
		try:
			projects = self.session.query(Project).order_by(Project.name).all()
			return projects
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	# Note: whenever we select a new project, a new session is started
	# not the same session that sqlalchemy uses - completely different thing!
	def setSelectedProject(self, projectID):
		try:
			# first check that the project is not already selected
			# there should only be one current session
			selectedProject = self.session.query(Project).filter(Project.current == 1).first()
			if selectedProject != None and selectedProject.id == projectID:
				return True
			else:
				# deselect the old projects
				oldProjects = self.session.query(Project).filter(Project.current == 1).all()
				for n in range(0, len(oldProjects)):
					oldProjects[n].current = 0
				self.session.commit()

				# we need to select the new project
				project = self.session.query(Project).filter(Project.id == projectID).first()
				project.current = 1
				self.addAndCommit(project)
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def isFileInProject(self, fileName, projectID):
		try:
			# note that in the context of directories, an absolute file name is unique
			fileData = self.session.query(File).filter(File.projectID == projectID, File.fileName == fileName)
			return fileData.count() == 1
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def getProjectsFromName(self, name):
		try:
			# there may be multiple projects with the same name
			projects = self.session.query(Project).filter(Project.name == name).all()
			return projects
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	# although not enforced as a primary key constraint, directory is unique
	# this comes as a characteristic of file systems
	def getProjectFromDirectory(self, directory):
		try:
			project = self.session.query(Project).filter(Project.directory == directory)
			if project.count() == 1:
				return project.first()
			else:
				return project.count()
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def projectExists(self, directory):
		project = self.getProjectFromDirectory(directory)
		return type(project) == Project

	def addProject(self, dictionary):
		try:
			newProject = Project(dictionary)
			self.addAndCommit(newProject)
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def deleteProject(self, directory):
		# directory can identify a project uniquely
		try:
			project = self.session.query(Project).filter(Project.directory == directory).first()
			if project == None:
				self.errors.throwApplicationError("Cannot remove project that isn't in the tracking database.\n"\
											+ "Note: if you have manually removed data without imposing the foreign key restrictions, "\
											+ "you can fix this by querying the tracking database manually.")
			else:
				self.session.delete(project)
				self.session.commit()
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def removeCodeFragmentsForFile(self, fileName):
		# fileName uniquely identifies the file (since it's absolute)
		# get id of file with file name
		# use project ID as well just in case files are shared
		selectedProject = self.getSelectedProject()
		fileObject = self.session.query(File).filter(File.fileName == fileName).first()
		if fileObject == None:
			self.errors.throwApplicationError("Cannot remove data for file that isn't in the tracking database.\n"\
											+ "Note: if you have manually removed data without imposing the foreign key restrictions, "\
											+ "you can fix this by querying the tracking database manually.")
		codeFragments = self.session.query(CodeFragment).filter(CodeFragment.fileID == fileObject.id).all()
		for n in range(0, len(codeFragments)):
			self.session.delete(codeFragments[n])
		self.session.commit()

	def addCodeFragment(self, dictionary, fileName):
		try:
			# check if the the feature belongs to the database
			# if it does, add newCodeFragment to its list of code fragements and commit
			# if not, add it and then do the same as above

			# use fileName of code fragment to get project ID (in file table)
			# then use feature name given in dictionary to get feature associated with project with id = project ID

			project = self.getSelectedProject()
			fileObject = self.session.query(File).filter(File.fileName == fileName, Project.id == project.id).first()
			# using project ID and feature name gives unique feature
			feature = self.session.query(Feature).filter(Feature.projectID == project.id, Feature.name == dictionary["feature"]).first()
			dictionary["fileID"] = fileObject.id
			if feature == None:
				# fill up missing keys that are compulsory for feature
				try:
					keyTest = dictionary["feature"]
				except KeyError as k:
					dictionary["feature"] = "unnamed feature"
				try:
					keyTest = dictionary["finished"]
				except KeyError as k:
					dictionary["finished"] = "no"
				try:
					keyTest = dictionary["tested"]
				except KeyError as k:
					dictionary["tested"] = "no"

				newFeature = Feature({
						"projectID" : project.id,
						"name" : dictionary["feature"],
						"finished" : dictionary["finished"],
						"tested" : dictionary["tested"]
					})
				self.addAndCommit(newFeature)
				# now, add the new code fragment to the feature and commit
				newCodeFragment = CodeFragment(dictionary)
				newFeature.codeFragments.append(newCodeFragment)
				self.session.commit()
			else:
				feature.codeFragments.append(CodeFragment(dictionary))
				self.session.commit()
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def addFile(self, dictionary):
		try:
			newFile = File(dictionary)
			self.addAndCommit(newFile)
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e

	def updateFile(self, dictionary):
		try:
			connection = self.engine.connect()
			connection.execute("update `file` set `numberOfLines` = %s, `hash` = '%s' where `fileName` = '%s' and `projectID` = %s"\
								% (dictionary["numberOfLines"], dictionary["hash"], dictionary["fileName"], self.getSelectedProject().id))
			connection.close()
		except Exception as e:
			self.errors.throwApplicationError(e, exitOnReport=True)
			return e			

# testing database controller
if __name__ == "__main__":
	controller = dbControl()