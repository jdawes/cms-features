#fragment:begin(name="feature name", ref="#1", finished="yes", "tested"="no")

class testClass:
	def __init__(self):
		print("This does some instantiation")
	def testFunction(self):
		print("This is a sample function")

	def __repr__(self):
		print("<testClass>")

#fragment:begin(name="another feature", ref="#1.1", finished="yes", "tested"="no")
print(testClass())
#fragment:end

#fragment:end
