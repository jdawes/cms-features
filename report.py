# This file contains a class that can be used to write reports from data stored about a project
# Format at the moment is html

import jinja2, os

class Report():

	handle = None
	database = None
	contentString = None
	fileName = None
	def __init__(self, fileName, databaseConnection):
		self.database = databaseConnection
		self.fileName = os.path.abspath(os.path.join(self.database.getSelectedProject().directory, fileName))

	def makeFileContent(self):
		# data can just be a list of sqlalchemy feature objects
		# from each one, we can get code fragments

		contentString = "<html>"
		contentString += "<head><style type=\"text/css\">body {font-family: arial;}</style></head>"
		contentString += "<body>"
		binaryToYesNo = {"1" : "&#10004;", "0" : "&times;"}
		booleanToYesNo = {True : "&#10004;", False : "&times;"}
		features = self.database.getFeaturesFromProject()
		for feature in features:
			contentString += "<h3>%s - Finished %s, Tested %s</h3>\n" % (feature.name, booleanToYesNo[self.database.isFeatureFinished(feature.id)],\
																		booleanToYesNo[self.database.isFeatureTested(feature.id)])
			for fragment in feature.codeFragments:
				fileObject = self.database.getFileFromID(fragment.fileID)
				contentString += "<div class='fragment'>"
				contentString += "<h4><i>code fragment</i> in file '%s' : %s</h4>\n" % (fileObject.fileName,\
														str(fragment.startLine) + "-" + str(fragment.endLine) if fragment.startLine != fragment.endLine else fragment.startLine)
				contentString += ("<p><i>Tested</i> : %s, <i>Finished:</i> %s</p>%s\n")\
								% (binaryToYesNo[fragment.tested], binaryToYesNo[fragment.finished], "<p>%s</p>" % fragment.comments if fragment.comments != "" else "")
				contentString += '</div>'

			contentString += "<hr>"
		contentString += "</body></html>"
		self.contentString = contentString

	def makeReport(self):
		self.handle = open(self.fileName, "w")
		self.handle.write(self.contentString)
		self.handle.close()
		return self.fileName