# Constructs declarative base for ORM

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()