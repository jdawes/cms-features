# handle logging

import logging, datetime, time
timeString = datetime.datetime.fromtimestamp(time.time()).strftime("%Y%m%d%H%M%S")
logging.basicConfig(filename="logs/log" + timeString, level=logging.DEBUG)

class Logger():
	filename = None
	def __init__(self, filename):
		self.filename = filename
	def formatMessage(self, message):
		return "[%s] %s" % (datetime.datetime.fromtimestamp(time.time()).strftime("%Y/%m/%d %H:%M:%S"), message)
	def write(self, message):
		logging.info(self.formatMessage(message))