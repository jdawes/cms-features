# command-line interpreter

# need this to add objects to the database
import os
import ormClasses
from databaseController import dbControl
from errorHandling import errorHandling
from parser import parser
import hashlib

# only one of these is needed
class commandInterpreter():

	database = None
	errors = None
	fileParser = None
	def __init__(self):
		self.database = dbControl()
		self.errors = errorHandling(None)
		self.fileParser = parser()

	def formatDirectory(self, directory):
		if directory == ".":
			return self.formatDirectory(os.getcwd())
		if directory[len(directory)-1] != "/":
			directory += "/"
		return directory

	def checkProject(self):
		# make sure a project is selected
		projectCheck = self.database.getSelectedProject()
		if type(projectCheck) == ormClasses.Project:
			#print("Project selected is '%s'." % projectCheck.name)
			# may add some kind of prompt here - will have to be configurable
			return projectCheck
		else:
			print("No single project is selected:\n"
				+ "\t- Run 'select project', which will attempt to select based on your directory,\n"\
				+ "\t- Run 'select project <project name>', which will select a project, possibly in another directory, or\n"
				+ "\t- To create a new project, first run 'add project <project name>' in the new project's root directory.")
			return False

	def checkProjectMatchesDirectory(self):
		currentWorkingDirectory = self.formatDirectory(".")
		# make sure a project is selected
		project = self.checkProject()
		if type(project) == ormClasses.Project:
			selectedProjectDirectory = project.directory
			return selectedProjectDirectory == currentWorkingDirectory[0:len(selectedProjectDirectory)]
		else:
			exit()

	# given a filename with a directory, remove the file's project directory from it
	# to obtain a directory relative to the project's root
	# uses selected project
	def relativeFileName(self, fileName):
		selectedProject = self.database.getSelectedProject()
		# make sure file isn't already relative
		if fileName[0] == "/" and fileName[0:len(selectedProject.directory)] == selectedProject.directory:
			return fileName[len(selectedProject.directory):]
		else:
			return fileName

	def inputToBoolean(self, string):
		mapping = {
			"yes" : True, "no" : False,
			"y" : True, "n" : False
		}
		try:
			return mapping[string]
		except KeyError as k:
			return None

	# Note: for adding things to a project, all we can do is add if a project is selected
	# it's up to the user to make sure they're adding to the correct project
	def addCommand(self, parameters):
		# category is either file, feature or service
		categoryToAdd = parameters[0]

		if len(parameters) == 1:
			# no additional data is provided that all the commands below require
			self.errors.throwApplicationError("Not enough arguments given for the 'add' action.")

		# decide what data we need next based on the category
		# we can't do checkProject here because add may be used to add a project...
		if categoryToAdd == "file":
			# first, do project check process
			# if a project is selected, it is returned through self.checkProject()
			project = self.checkProject()
			if type(project) == ormClasses.Project:
				if parameters[1] == "all":
					print("Finding all files in the project directory, and parsing.  This may take a while...\n")
					# use os.walk to build a list of files that we can add
					for root, dirs, files in os.walk(project.directory):
						for fileName in files:
							parameters.append(self.relativeFileName(os.path.join(root, fileName)))
					parameters = parameters[2:]
				# add all files at index > 1 in the parameters list
				for n in range(1, len(parameters)):
					# if a relative path is given, append this to the project pat
					fileName = parameters[n]
					relativePath = fileName[0] != "/"
					projectPath = project.directory

					if relativePath:
						# find "intermediate path" leading to fileName from the root project path
						newFileName = self.formatDirectory(os.getcwd()) + fileName
					else:
						# since if not relative, then it must be absolute
						newFileName = fileName

					newFileName = os.path.abspath(newFileName)

					# Note: allow files from non-project directories
					# sometimes files in completely different directories are used

					if not(os.path.isfile(newFileName)):
						self.errors.throwApplicationError("'%s' is not a file." % newFileName, exitOnReport=False)
						continue

					# make sure the file exists
					try:
						open(newFileName, "r")
						fileExists = True
					except IOError as io:
						# either the file is locked (unlikely) or it doesn't exist
						fileExists = False

					if not(fileExists):
						self.errors.throwApplicationError("The file '%s' doesn't exist." % newFileName, exitOnReport=True)
						exit()
					else:
						# before parsing, make sure we don't already have this file
						fileInProject = self.database.isFileInProject(newFileName, project.id)
						if fileInProject:
							print("You have already added file '%s'." % self.relativeFileName(newFileName))
						else:
							fragments = self.fileParser.parse(fileName)
							if fragments == None:
								continue
							fragmentData = fragments["fragments"]
							numberOfLinesInFile = fragments["numberOfLines"]
							# add file to database
							# do it here so that the parser can quit if any errors are found
							self.database.addFile({
									"fileName" : newFileName,
									"projectID" : project.id,
									"numberOfLines" : numberOfLinesInFile,
									"hash" : commandInterpreter.getFileHash(newFileName)
								})
							#print("Parsing file to extract code fragments, and adding data to database.")
							#print("File '%s' added." % newFileName)
							for fragment in fragmentData.values():
								self.database.addCodeFragment(fragment, newFileName)
							print("File '%s' being tracked." % newFileName)
							#print("Code Fragment and Feature objects added to tracking database.")
				print("All valid files added to tracking database.")
		elif categoryToAdd == "project":
			# add project with name and absolute directory
			# since the directory column isn't a primary key, check for the project existing first
			# we only need to be concerned with the directory of the project
			projectName = parameters[1]
			# means you have to navigate to the directory to set up a project there
			# add trailing slash to directory if we need one
			directory = self.formatDirectory(".")
			directory = directory[0:len(directory)-1]
			# make sure no project exists in any parent directory of this
			# I don't think it's necessary to allow projects inside projects
			# construct list of directory parts and throw away the first element
			directoryParts = directory.split("/")[1:]
			directoryString = "/"
			projectExists = False
			for part in directoryParts:
				directoryString += part + "/"
				projectExists = self.database.projectExists(directoryString)
				if projectExists:
					break
			if projectExists:
				print("A project already exists in the directory '%s'." % directoryString)
			else:
				# add a new project
				self.database.addProject({
						"name" : projectName,
						"directory" : directoryString,
						"current" : 0
					})
				print("Project '%s' has been added in directory '%s'." % (projectName, directoryString))
				print("Note: If you have created a project inside a parent directory of an existing project, "\
					+ "you may not get the result you expect when autoselecting a project based on working directory.\n"\
					+ "It can also result in the wrong file records being edited in the database.")

	def removeCommand(self, parameters):
		categoryToRemove = parameters[0]

		if categoryToRemove == "file":
			if len(parameters) == 1:
				# no additional data is provided that all the commands below require
				self.errors.throwApplicationError("Not enough arguments given for the 'remove file' action.")
			# accept multiple file names
			for n in range(1, len(parameters)):
				# orm classes are set to cascade delete when foreign key records are deleted,
				# so deleting a file will automatically delete its code fragments
				fileName = os.path.abspath(self.formatDirectory(os.getcwd()) + parameters[n])
				result = self.database.deleteFile(fileName)
				if type(result) != Exception:
					print("File '%s' has been removed, along with all code fragments belonging to it." % fileName)
				else:
					self.errors.throwApplicationError(result, exitOnReport=True)

		elif categoryToRemove == "project":
			# again, orm classes are set to cascade delete when foreign key records are deleted,
			# so deleting a project will delete all features, all files and all code fragments
			# we should probably make sure with the user about this one - reparsing all their project files may
			# take a while

			projectCheck = self.checkProject()
			if not(projectCheck):
				return False
			selectedProject = self.database.getSelectedProject()
			choice = self.inputToBoolean(
				raw_input(("Delete the selected project '%s' in directory '%s'?\n" % (selectedProject.name, selectedProject.directory))\
						+ "This will delete all features, files and code fragment objects from the tracking database "\
						+ "(NOT from your filesystem).\n"\
						+ "Type 'yes' or 'no': ")
				)
			if not(choice):
				print("Your project has NOT been deleted.")
				exit()

			# to help with reliability of deleting a project, delete the selected project
			# this forces the user to select a project first, but makes sure they are deleting the correct one
			result = self.database.deleteProject(selectedProject.directory)
			if type(result) == Exception:
				print("There was a problem deleting your project.\n"\
					+ "If this keeps happening, and isn't some error with permissions on the database file, "\
					+ "you can just use sqlite to query the /data/services/featureControl/tracking.db file yourself."\
					+ "Instructions can be found in documentation.")
			else:
				print("Project, and all objects belonging to it, removed from the tracking database.")

	def queryCommand(self, parameters):
		categoryToQuery = parameters[0]

		# querying which project is selected
		if categoryToQuery == "project":
			# check that a project has been selected
			project = self.checkProject()
			if type(project) != ormClasses.Project:
				exit()
			# query for which project is selected
			print("\nSelected project is '%s' in directory '%s'." % (project.name, project.directory))
			print("Use 'select project' or 'select project <project name>' to switch project.\n")

		if categoryToQuery == "files":
			# check that a project has been selected
			project = self.checkProject()
			if type(project) != ormClasses.Project:
				exit()
			files = self.database.getFilesForSelectedProject()
			if len(files) > 0:
				print("\nFiles known about in '%s':" % project.name)
				for fileObject in files:
					if len(parameters) > 1 and parameters[1] == "absolute":
						prefix = project.directory
					else:
						prefix = ""
					print("\t%s%s" % (prefix, self.relativeFileName(fileObject.fileName)))
			else:
				print("\nNo files added to project '%s'." % project.name)
			print("Note: files not in the current project directory (external dependencies) are given with their absolute paths.")
			print("")

		if categoryToQuery == "features":
			# check that a project has been selected
			project = self.checkProject()
			if type(project) != ormClasses.Project:
				exit()
			if len(parameters) > 1:
				if parameters[1] == "tree":
					# any point in implementing this?
					# the idea is a visual tree - rather than the hierarchy you get already
					pass
			else:
				# will get features from selected project
				features = self.database.getFeaturesFromProject()
				if len(features) > 0:
					print("\nFeatures built by parser for project '%s':\n" % project.name)
					for feature in features:
						fragments = self.database.getContributingFragments(feature.id)
						numberOfFiles = self.database.getTotalFilesFromFragments(fragments)
						finished = "finished" if self.database.isFeatureFinished(feature.id) else "not finished"
						tested = "tested" if self.database.isFeatureTested(feature.id) else "not tested"
						print("\t%s [contributing code in %s files] [%s, %s]:\n"\
							 % (feature.name, numberOfFiles, finished, tested))
						# print out indented list of all fragments contributing to this feature, and in which files

						for fragment in fragments:
							fileObject = self.database.getFileFromID(fragment.fileID)
							singleLine = fragment.startLine == fragment.endLine
							if fileObject != None:
								if singleLine:
									print("\t\tfragment with label '%s' in file '%s' on line %s"\
									% (fragment.ref,
									   self.relativeFileName(fileObject.fileName),
									   fragment.startLine))
								else:
									print("\t\tfragment with label '%s' in file '%s' on lines %s-%s"\
										% (fragment.ref,
										   self.relativeFileName(fileObject.fileName),
										   fragment.startLine, fragment.endLine))
							else:
								print("\t\tA fragment is in the database that doesn't belong to any file.")
						print("\t" + "".join(["_"]*100))
						print("")
				else:
					print("\nNo features have been found during parsing in project '%s'.\n" % project.name)
				print("Note: If you haven't run the parse command on the project/any files and you've made changes, "\
						+ "this data may not be accurate.\n")
		if categoryToQuery == "projects":
			# list all projects
			projects = self.database.getAllProjects()
			#selectedProject = self.database.getSelectedProject()
			if len(projects) > 0:
				print("\nAll known projects:")
				for project in projects:
					# this throws the no project selected error
					# and this command helps to select a project - so this feature shouldn't be used
					"""if type(selectedProject) == ormClasses.Project:
						if project.id == self.database.getSelectedProject().id:
							current = " [selected]"
						else:
							current = ""
					else:
						current = ""
					"""
					print("\t'%s' in directory '%s'" % (project.name, project.directory))
				print("")
			else:
				print("You haven't added any project yet. "\
					+ "Run 'add project <project name>' while in the project's directory to create a project.")

		if categoryToQuery == "feature":
			if len(parameters) == 1:
				# no additional data is provided that all the commands below require
				self.errors.throwApplicationError("Not enough arguments given for the 'query feature' action.")

			featureName = parameters[1]
			# get selected project id, and use feature name with this to uniqely determine feature
			# then get all fragments with feature ID
			selectedProject = self.database.getSelectedProject()
			fragments = self.database.getFragmentsForFeature(featureName)
			# if there are no fragments, an exception is thrown in databaseController.py
			feature = self.database.getFeatureFromID(fragments[0].featureID)
			# important that finished and tested are determined by checking code fragments
			finished = "finished" if self.database.isFeatureFinished(feature.id) else "not finished"
			tested = "tested" if self.database.isFeatureTested(feature.id) else "not tested"
			print("\nCode fragments found during parsing contributing to feature '%s' [%s, %s]:\n"\
				% (featureName, finished, tested))
			if len(fragments) > 0:
				for fragment in fragments:
					fileObject = self.database.getFileFromID(fragment.fileID)
					fragmentFinished = (["not finished", "finished"])[int(fragment.finished)]
					fragmentTested = (["not tested", "tested"])[int(fragment.tested)]
					print("\tfragment with label '%s' in file '%s' on lines %s-%s"\
										% (fragment.ref,
										   self.relativeFileName(fileObject.fileName),
										   fragment.startLine, fragment.endLine))
					print("\t\tComments: \"%s\"" % fragment.comments)
					print("\t\t%s, %s" % (fragmentFinished, fragmentTested))
				print("")
			else:
				print("\nNo fragments exist for feature '%s'.\n" % featureName)

		if categoryToQuery == "properties":
			# use parser to parse list into dictionary, and use getFragmentsWithConfig in databaseController.py
			# join arguments together into string ready for parsing
			try:
				argumentString = ",".join(parameters[1:])
				argumentsAsDictionary = self.fileParser.parseParameterList(argumentString, refsRequired=False)
				for key in argumentsAsDictionary:
					if key in ["finished", "tested"]:
						argumentsAsDictionary[key] = "'%s'" % ormClasses.valueToInteger[argumentsAsDictionary[key]]
					else:
						argumentsAsDictionary[key] = "'%s'" % argumentsAsDictionary[key]
			except KeyError as k:
				self.errors.throwApplicationError("A search parameter's value was incorrect.")
			fragments = self.database.getFragmentsWithConfig(argumentsAsDictionary)
			# print to screen
			print("Fragments found by search:")
			searchStringForDisplay = ""
			for key in argumentsAsDictionary:
				searchStringForDisplay += key + " = " + argumentsAsDictionary[key] + "\t"
			print("\t" + searchStringForDisplay + "\n")
			if len(fragments) > 0:
				for fragment in fragments:
					fileObject = self.database.getFileFromID(fragment.fileID)
					singleLine = fragment.startLine == fragment.endLine
					feature = self.database.getFeatureFromID(fragment.featureID)
					if fileObject != None:
						if singleLine:
							print("\tfragment of feature '%s' with label '%s' in file '%s' on line %s"\
							% (feature.name,
							   fragment.ref,
							   self.relativeFileName(fileObject.fileName),
							   fragment.startLine))
						else:
							print("\tfragment of feature '%s' with label '%s' in file '%s' on lines %s-%s"\
								% (feature.name,
								   fragment.ref,
								   self.relativeFileName(fileObject.fileName),
								   fragment.startLine, fragment.endLine))
					else:
						print("\tA fragment is in the database that doesn't belong to any file.")
			else:
				print("No code fragments found with these properties.\n")
			print("")

		if categoryToQuery == "non-fragment-code":
			# Work out better way to do this - it's inaccurate at the moment

			# finds all code that isn't in a fragment by taking line ranges of tracked fragments from the range of lines
			# stored about the file
			# this will all be subject to what is stored in the tracking database

			project = self.checkProject()
			if type(project) != ormClasses.Project:
				exit()

			# get all files belonging to this project
			allFiles = self.database.getFilesForSelectedProject()
			for fileObject in allFiles:
				# compute list of lines being tracked from code fragments
				# take that list from the range stored in this file
				# and the is the list of untracked files

				trackedLines = []

				fragmentsOfCurrentFile = self.database.getFragmentsInFile(fileObject.fileName)
				for fragment in fragmentsOfCurrentFile:
					startLine = fragment.startLine
					endLine = fragment.endLine
					trackedLines += list(range(startLine, endLine+1))

				untrackedLines = list(set(range(1, fileObject.numberOfLines+1)) - set(trackedLines))

				untrackedRanges = []
				# split list into list of intervals
				start = 0
				end = 0
				for n in range(0, len(untrackedLines)):
					if n < len(untrackedLines)-1:
						if untrackedLines[n+1] != untrackedLines[n]+1:
							untrackedRanges.append([untrackedLines[start], untrackedLines[end]])
							start = n+1
							end = n+1
						else:
							end = n
					elif n == len(untrackedLines)-1:
						untrackedRanges.append([untrackedLines[start], untrackedLines[end]])

				print("'%s' has non-fragment code at %s" % (fileObject.fileName, untrackedRanges))
			print("Note: This data may be slightly inaccurate due to the way lines are mapped during parsing.")


	def selectCommand(self, parameters):
		# at the moment, just for selecting a project
		categoryToSelect = parameters[0]

		# the only category at the moment
		if categoryToSelect == "project":
			if len(parameters) == 1:
				print("Selecting closest project in this or parent directories.")
				# we haven't been given a project name
				# so attempt to find the project by iterating through levels of the current working directory
				currentWorkingDirectory = os.getcwd()
				# remove the first element, since the directory starts with
				parts = currentWorkingDirectory.split("/")[1:]
				# loop through the parts list and query the database 
				directoryString = self.formatDirectory(currentWorkingDirectory)
				projectFound = False
				# traverse directory tree from current directory to root
				for n in range(0, len(parts)):
					directoryString = "/" + self.formatDirectory("/".join(parts[0:len(parts)-n]))
					# query the database for a project with this directory
					project = self.database.getProjectFromDirectory(directoryString)
					if type(project) == ormClasses.Project:
						# a project exists in this directory - stop the loop and use this project
						self.database.setSelectedProject(project.id)
						projectFound = True
						break
				if projectFound:
					print("Selected project '%s' in directory '%s' based on current working directory." %\
							(project.name, project.directory))
				else:
					print("No project exists in your current directory, or any parent directories.\n"\
						+ "Note that this can just mean your project is in a sibling directory.")
			elif len(parameters) > 1:
				# project name must be given
				if len(parameters) == 1:
					# no additional data is provided that all the commands below require
					self.errors.throwApplicationError("Not enough arguments given for the 'select' action.")
				projectName = parameters[1]
				# if there are multiple projects of this name, list directories and ask for input
				projects = self.database.getProjectsFromName(projectName)
				if len(projects) == 1:
					# select the project
					self.database.setSelectedProject(projects[0].id)
					print("Project in directory '%s' selected." % projects[0].directory)
				elif len(projects) > 1:
					# list projects
					print("Projects with different directories were found with this name:")
					for n in range(0, len(projects)):
						if projects[n].current == 1:
							suffix = "[selected]"
						else:
							suffix = ""
						print("\t%s) %s %s" % (n+1, projects[n].directory, suffix))
					try:
						indexOfProject = -1
						while not(indexOfProject in range(1, len(projects)+1)):
							indexOfProject = int(raw_input("Which project did you mean? Type the number of your project: "))
					except KeyboardInterrupt as k:
						self.errors.throwApplicationError("Program ended", exitOnReport=True)
					project = projects[indexOfProject-1]
					self.database.setSelectedProject(project.id)
					print("Project in directory '%s' selected." % project.directory)
				elif len(projects) == 0:
					print("No projects were found with this name.\n"\
						+ "If you know the directory of your project, use 'select project' while inside that directory "\
						+ "and the project inside that directory will be detected.")

	def generateCommand(self, parameters):
		categoryToSelect = parameters[0]

		if categoryToSelect == "report":
			if len(parameters) < 2:
				self.errors.throwApplicationError("Not enough arguments given for the 'generate' action.")
			fileName = parameters[1]
			selectedProject = self.database.getSelectedProject()
			print("Generating report for project '%s'." % selectedProject.name)

			import report
			reportObject = report.Report(fileName, self.database)
			reportObject.makeFileContent()
			finalFileName = reportObject.makeReport()
			print("Report generated in file '%s' for project '%s'." % (finalFileName, selectedProject.name))

	def loadFileContent(self, fileName):
		try:
			handle = open(fileName, "r")
			lines = handle.readlines()
			return "".join(lines) + "\n"
		except IOError as io:
			self.errors.throwApplicationError(io, exitOnReport=True)
			return io

	def helpCommand(self, parameters):
		actionToHelpWith = parameters[0]

		# load help file into terminal
		helpFileContent = self.loadFileContent("/data/services/featureControl/help/" + actionToHelpWith)
		print("Help for action '%s':\n" % actionToHelpWith)
		print(helpFileContent)

	def parseCommand(self, parameters):
		selectedProject = self.database.getSelectedProject()

		fileToParse = os.path.abspath(parameters[0])

		# make sure this file is in the project first
		selectedProject = self.database.getSelectedProject()
		fileInProject = self.database.isFileInProject(fileToParse, selectedProject.id)
		if not(fileInProject):
			self.errors.throwApplicationError("The file you're trying to parse is not in the project '%s'." % selectedProject.name)
		fragmentData = self.fileParser.parse(fileToParse)
		# for now, remove all fragments belonging to this file,
		# and then run addCodeFragment from databaseController.py with the new fragments data
		print("Removing all fragments belonging to file '%s'." % fileToParse)
		self.database.removeCodeFragmentsForFile(fileToParse)

		# now construct objects for the updated fragments
		print("Constructing new fragment objects and adding to tracking database.")
		for fragment in fragmentData["fragments"].values():
			self.database.addCodeFragment(fragment, fileToParse)

		# update file
		self.database.updateFile({"fileName" : fileToParse, "numberOfLines" : fragmentData["numberOfLines"], "hash" : commandInterpreter.getFileHash(fileToParse)})

		print("Parsing complete for file '%s'." % fileToParse)
		print("Run 'query features' to see updated feature and code fragment data.")

	@staticmethod
	def getFileHash(fileName):
		try:
			handle = open(fileName, "r")
			contents = "".join(handle.readlines())
			hash = hashlib.sha1(contents).hexdigest()
			return hash
		except IOError as io:
			errorHandling().throwApplicationError("File couldn't be opened while computing hash.", exitOnReport=True)

	def parseProjectCommand(self, parameters):
		selectedProject = self.database.getSelectedProject()
		# get all files belonging to project
		allFiles = self.database.getFilesForSelectedProject()
		if len(allFiles) > 0:
			print("\nParsing and updating code fragment objects in tracking database for:\n")
			counter = 1
			for fileObject in allFiles:
				fragmentData = self.fileParser.parse(fileObject.fileName)
				print("\t(%s/%s) %s [adding %s code fragments]"\
					% (counter, len(allFiles), self.relativeFileName(fileObject.fileName), len(fragmentData)))
				self.database.removeCodeFragmentsForFile(fileObject.fileName)
				for key in fragmentData:
					self.database.addCodeFragment(fragmentData[key], fileObject.fileName)
				counter+=1
			print("\nFinished parsing all known files in project '%s'.\n" % selectedProject.name)
		else:
			print("\nNo files added to project '%s'.\n" % selectedProject.name)

	def parseArguments(self, commandList):
		if len(commandList) == 1:
			self.errors.throwApplicationError("Not enough arguments given.", exitOnReport=True)
		if commandList[1] != "parse-project" and len(commandList[2:]) == 0:
			self.errors.throwApplicationError("Not enough arguments given.", exitOnReport=True)
		# split command string up and decide if it's correct
		# and what we need to do
			

		# most general action is the first one
		# since Python doesn't have a switch construct (not that I'm aware of, anyway)
		# we use a dictionary of functions

		switchDictionary = {
			"add" : self.addCommand,
			"remove" : self.removeCommand,
			"query" : self.queryCommand,
			"select" : self.selectCommand,
			"parse" : self.parseCommand,
			"parse-project" : self.parseProjectCommand,
			"generate" : self.generateCommand,
			"help" : self.helpCommand
		}

		mainAction = commandList[1]
		if not("query" in commandList) and not("project" in commandList) and not("help" in commandList) and not("generate" in commandList)\
		   and not("parse-project" in commandList) and not("parse" in commandList):
			# check that the working directory is the same as the project selected
			match = self.checkProjectMatchesDirectory()
			if not(match):
				# tell the user
				print("\nYou are not inside the directory of the selected project.\n")
				exit()

		mainActionFunction = switchDictionary[mainAction]
		mainActionFunction(commandList[2:])

if __name__ == "__main__":
	import sys
	interpreter = commandInterpreter()
	interpreter.parseArguments(sys.argv)