# Parser class

"""

- readLines(fileName)	will read in a list of the lines in fileName.
- relabelLines(lines)	lines will be a list of pairs (line number, line content), and this function will loop through
						lines and, for each one, change line number to the line number of the next non-comment line.
- directiveLines(lines)	takes a list of lines that have been changed by relabelLines to have the correct line numbers.

"""

import os
# regular expressions for extracting data from parameter list in comments of code
import re
from errorHandling import errorHandling

# Parser contains functions that read lines in from a source file,
# process them so we only have feature control comments,
# and then parses comments to construct code fragment objects.
class parser():

	lines = None
	queue = None
	#projectName = None
	errors = None
	fragmentDictionaries = None
	currentFileName = None
	commentSymbol = "#"
	numberOfLines = 0
	fileExtensionToCommentSymbol = {
		"py" : "#",
		"html" : "<!--",
		"css" : "/*",
		"js" : "//"
	}

	def __init__(self):
		# note that projectName is a primary key in the project table
		# so this is enough to uniquely identify it
		# all data will be passed to another file that knows the project name
		pass

	def clean(self, string):
		string = string.lstrip().rstrip()
		replaceWithEmpty = ["\t"]
		for character in replaceWithEmpty:
			string = string.replace(character, "")
		return string

	def readLines(self, fileName):
		try:
			# we never open a file with write permission
			handle = open(fileName, "r")
			# construct list of lines
			lines = map(lambda line : self.clean(line), handle.readlines())
			lineNumber, n = 0, 0
			# use a for loop so termination of the loop is sorted out for us
			for line in lines:
				lineNumber += 1
				"""if line == "":
					continue
				else:"""
				lines[n] = [n+1, line, lineNumber]
				n += 1
				self.numberOfLines += 1
			self.lines = lines[0:n]
		except IOError as e:
			# replace this by logging
			exceptionString = str(e)
			self.errors.throw(exceptionString)
			return exceptionString

	def nextNonCommentLine(self, lineNumber, commentChars):
		for n in range(lineNumber, len(self.lines)):
			if len(self.lines[n][1]) == 0:
				continue
			if self.lines[n][1][0:len(commentChars)] != commentChars:
				return n+1
		return len(self.lines)

	def previousNonCommentLine(self, lineNumber, commentChars):
		for n in range(0, lineNumber):
			index = lineNumber-n
			if len(self.lines[index][1]) == 0:
				continue
			if self.lines[index][1][0:len(commentChars)] != commentChars:
				return index+1
		return 0

	def nextLineWithCloseBracket(self, lineNumber):
		for n in range(lineNumber+1, len(self.lines)):
			if len(self.lines[n][1]) == 0:
				continue
			if self.lines[n][1][len(self.lines[n][1])-1] == ")":
				return n
		self.errors.queue("Code fragment was never ended.", self.currentFileName)
		return len(self.lines)

	def isFragmentBeginLine(self, line):
		# check against all comment symbols
		# iterate through keys of file extension dictionary
		for commentChars in self.fileExtensionToCommentSymbol:
			commentChars = self.fileExtensionToCommentSymbol[commentChars]
			if line[0:(len(commentChars) + 14)] == commentChars + "fragment:begin":
				return True
		return False

	def isFragmentEndLine(self, line):
		for commentChars in self.fileExtensionToCommentSymbol:
			commentChars = self.fileExtensionToCommentSymbol[commentChars]
			if line == commentChars + "fragment:end":
				return True
		return False

	def getTypeOfStatement(self, line):
		for commentChars in self.fileExtensionToCommentSymbol:
			commentChars = self.fileExtensionToCommentSymbol[commentChars]
			if line[0:(len(commentChars) + 14)] == commentChars + "fragment:begin":
				return ["begin", commentChars]
			elif line == commentChars + "fragment:end":
				return ["end", commentChars]
		return [None, None]

	def relabelLines(self):
		# for each comment line, find the next non-comment line
		# assuming lines has been passed through readLines

		finalListOfLines = []

		# N iterations, N = len(self.lines)
		for n in range(0, len(self.lines)):
			# hard coding syntax will probably change
			# instead of checking for begin then end, check for both at the same time
			# brings efficiency down from O(2M) (M = number of file extensions) to O(M)
			typeOfStatement = self.getTypeOfStatement(self.lines[n][1])
			if typeOfStatement[0] == "begin":
				self.lines[n][0] = self.nextNonCommentLine(n, typeOfStatement[1])
				# we also need to check if this line ends with )
				# if not, find the next line that does (it will be the last character since whitespace has been stripped)
				if self.lines[n][1][len(self.lines[n][1])-1] != ")":
					# fine the next line that ends with a bracket
					numberOfNextLine = self.nextLineWithCloseBracket(n)
					self.errors.processQueue(self.lines[n][2])
					# now merge the lines by removing #, stripping whitespace, and adding onto this line
					for m in range(n+1, numberOfNextLine+1):
						withoutHashAndSpace = self.clean(self.lines[m][1][len(self.commentSymbol):])
						self.lines[n][1] += withoutHashAndSpace
				finalListOfLines.append(self.lines[n])
			elif typeOfStatement[0] == "end":
				self.lines[n][0] = self.previousNonCommentLine(n, typeOfStatement[1])
				finalListOfLines.append(self.lines[n])
		self.lines = finalListOfLines

	# evalutes individual expressions used in parameters
	def evaluate(self, expression):
		# strip string quote away
		if expression[0] == '"' and expression[len(expression)-1] == '"':
			expression = expression[1:len(expression)-1]
		return expression

	def parseParameterList(self, string, refsRequired=False, lineNumber=None):
		# loop through string, removing spaces when we aren't inside quotes
		inQuotes = False
		currentQuotePosition = 0
		newString = ""
		for n in range(0, len(string)):
			if not(inQuotes) and string[n] == " ":
				continue
			if string[n] == '"':
				inQuotes = not(inQuotes)
				if inQuotes:
					currentQuotePosition = n
			if string[n] == " " and inQuotes:
				newString += string[n]
			elif string[n] != " ":
				newString += string[n]
		if inQuotes:
			# we shouldn't be - this is incorrect syntax
			self.errors.queue("Quote at position %s was never closed."\
							  % currentQuotePosition, self.currentFileName)
			return None
		string = newString
		# now spaces outside quotes have been removed
		# iterate through the string again and split by commas
		# we have to iterate because there might be commas inside quotes
		# if we're reached this point, inQuotes has to be false anyway

		inQuotes = False
		startPoint, endPoint = 0, 0
		sections = []
		for n in range(0, len(string)):
			endPoint = n
			if string[n] == '"':
				inQuotes = not(inQuotes)
			if not(inQuotes) and string[n] == ",":
				sections.append(string[startPoint:endPoint])
				startPoint = endPoint+1
			elif not(inQuotes) and n == len(string)-1:
				sections.append(string[startPoint:])

		# now we have sections, split them up by =
		# errors we can get here are that each section does not split into exactly two parts
		# we could map a lambda function onto the sections list, but we want to inspect individual elements

		for n in range(0, len(sections)):
			parts = sections[n].split("=")
			if len(parts) != 2:
				self.errors.queue("Parameter %s incorrectly defined - should be param=value (%s)"\
								  % (n, sections[n]), self.currentFileName)
				return None
			sections[n] = sections[n].split("=")

		# now strip quotes from any parameter values (this may need to be more powerful
		# than just stripping quotes at some point)

		for n in range(0, len(sections)):
			# if there is an uneven number of quotes, we would have gotten an error before
			if sections[n][1][0] == '"' and sections[n][1][len(sections[n][1])-1] == '"':
				sections[n][1] = sections[n][1][1:-1]

		paramsDictionary = dict(sections)
		paramsDictionary["ref"] = lineNumber
		if refsRequired:
			try:
				refsTest = paramsDictionary["ref"]
			except KeyError as k:
				self.errors.queue("The refs parameter was not defined.", self.currentFileName)
		return paramsDictionary

	def processPairs(self):
		# process self.lines once self.readLines() and self.relabelLines() have been called
		self.queue = []
		self.fragmentDictionaries = {}
		newFeature = None
		for pair in self.lines:
			# look at the parameters given after the first "("
			typeOfStatement = self.getTypeOfStatement(pair[1])
			if typeOfStatement[0] == "begin":
				# get and process the parameters given
				# get position of closing bracket
				# extract parameter list string
				parameterListString = pair[1].split("fragment:begin(")[1]
				closingBracketIndex = parameterListString.index(")")
				parameterListString = parameterListString[0:closingBracketIndex]
				parameters = self.parseParameterList(parameterListString, refsRequired=True, lineNumber=pair[0])
				# there may be an error in the queue, so process it here
				self.errors.processQueue(pair[2])
				parameters["startLine"] = pair[0]
				self.queue.append(parameters)
				# is enforcing the ref parameter a good idea?
				# at the moment, it's helpful
				self.fragmentDictionaries[pair[2]] = parameters
				# now build a code fragment object with the parameters taken from the code,
				# and push it to the queue
				# the next time we encounter #fragment:end, this object is updated, pulled from the queue
				# and will be added to the database via the ORM
			if typeOfStatement[0] == "end":
				newFragment = self.queue.pop()
				newFragment["endLine"] = pair[0]
				# case in which fragment of code is empty - there's no point in storing it
				if int(newFragment["startLine"]) > int(newFragment["endLine"]):
					del self.fragmentDictionaries[pair[2]]
				# to make sure we have the updated version of the fragment
				#self.fragmentDictionaries[newFragment["ref"]] = newFragment


	def parse(self, fileName):
		self.numberOfLines = 0
		self.errors = errorHandling(fileName)
		self.queue = []
		self.lines = None
		self.currentFileName = fileName
		extension = fileName.split(".")
		extension = extension[len(extension)-1]
		try:
			self.commentSymbol = self.fileExtensionToCommentSymbol[extension]
		except KeyError as k:
			print("File '%s' extension %s not supported." % (fileName, extension))
			return None
		# will populate self.lines with lists of three elements:
		#	1) the line number in non-comment code the fragment statement maps to (not accurate here)
		#	2) the fragment statement
		#	3) the original line number of the fragment statement
		self.readLines(fileName)

		"""print("")
		for line in self.lines:
			print(line)
		print("")"""

		# makes 1) from readLines correct by searching either forwards or backwards
		# if one statement is over multiple lines, merge into one line

		self.relabelLines()

		"""print("")
		for line in self.lines:
			print(line)
		print("")"""

		# parse the parameter list, and construct a dictionary of fragment data with refs as keys
		self.processPairs()
		# now we have a dictionary of fragment data, as well as refs, we can update the database (as long as refs stay the same)
		# will have to handle refs changing
		return {"fragments":self.fragmentDictionaries, "numberOfLines":self.numberOfLines}

if __name__ == "__main__":
	#from logger import Logger
	import sys

	# for now, hard code the project name
	parserObject = parser("test project")

	# for now just print to the console
	#log = Logger("log")
	#log.write(parserObject.readLines(sys.argv[1]))
	fragmentDictionaries = parserObject.parse(sys.argv[1])
	print("Parsing complete - fragments constructed are:")
	print(fragmentDictionaries)