"""

Can be run in the background - for each loop, will get the selected project (if none is selected, continue to next iteration),
get all files in that project, and get their hashes.  If the hash is different to the one in the db, the file should be parsed.

"""

import databaseController as dbControl
import ormClasses
import commandInterpreter as cmd
import time
from parser import parser

try:

	while True:
		database = dbControl.dbControl()
		# get selected project
		selectedProject = database.getSelectedProject()

		if selectedProject == None:
			# continue to next iteration and try again
			continue

		# get files for selected project
		files = database.getFilesForProject(selectedProject.id)

		for fileObject in files:
			print("Checking file '%s'." % fileObject.fileName)
			# get stored hash of file
			hash = fileObject.hash
			print(hash)

			# compute actual hash of file
			newHash = cmd.commandInterpreter.getFileHash(fileObject.fileName)
			print(newHash)

			if hash != newHash:
				# reparse the file
				print("Detected change in file '%s' - reparsing." % fileObject.fileName)
				cmd.commandInterpreter().parseArguments(["", "parse", fileObject.fileName])

		time.sleep(5)

except KeyboardInterrupt:
	exit("Interrupted by keyboard input.  Some data in tracking database may be incorrect.")