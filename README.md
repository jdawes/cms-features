## Using cms-features

1. Add `alias cms-features='python /data/services/featureControl/cms-features.py'` to your ~/.bashrc file.
2. Use `cms-features help <action>` where action is either *add*, *remove*, *query*, *select*, *generate*.
3. See slides for notes on how to annotate code:

Slides: https://indico.cern.ch/event/369579/contribution/0/attachments/1135562/1624881/feature-tracking-slides.pdf

General way to annotate code:

```
<comment symbol>fragment:begin(name="...", feature="...", comments="...", finished="yes/no", tested="yes/no")
code contributing to feature
<comment symbol>fragment:end
```

where <comment symbol> is the comment symbol of the language of the code that you're annotating.

Note that all comment symbols are checked for all files, eg so HTML files with JS and CSS are supported.