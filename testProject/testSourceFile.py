#fragment:begin(name="feature 1",
#				ref="#1",
#				finished=no,
#				tested=no,
#				comments="Some comment about the code in this code fragment")
#fragment:begin(name="feature 3", ref="#2", finished=no, tested=no)

print("some information")
print("this code contributes to feature 1")

#fragment:end

print("some extra code here")
print("This separates the features a bit")

#fragment:end

#fragment:begin(name="feature 2", ref="#3", finished=yes, tested=no)

print("some code that does feature 2")

#fragment:end

#fragment:begin(name="feature 3", ref="#4", finished=yes, tested=yes)
#fragment:begin(name="feature 2", ref="#5", finished=no, tested=no)

for n in range(0, 10):
	print(n)

subTotal = 0
for n in range(0, 100):
	subTotal += n**2

#fragment:end
#fragment:end
