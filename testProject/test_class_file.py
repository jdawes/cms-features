import sys, os
#fragment:begin(name="feature 2", finished=no, tested=no, ref="#1")

for n in range(0, len(sys.argv)):
	print(sys.argv[n])

# open a file
try:
	handle = open("testFile", "r")
	# can't remember if this is the right function for this
	line = handle.read()
except IOError as e:
	print(e)

#fragment:end

#fragment:begin(name="feature 1", finished=yes, tested=yes, ref="#2")

def functionForFeature1():
	print("This is a function!")

#fragment:end
