create table project (
	id integer primary key not null,
	name text not null,
	directory text not null,
	current integer not null
);