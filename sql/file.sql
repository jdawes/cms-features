create table file (
	id integer primary key not null,
	fileName text not null,
	projectID text not null,
	numberOfLines integer not null,
	hash text not null,
	foreign key(projectID) references project(id)
);