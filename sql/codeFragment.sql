create table codeFragment (
	id integer primary key not null,
	finished varchar(1) not null,
	tested varchar(1) not null,
	comments text not null,
	startLine integer not null,
	endLine integer not null,
	ref text not null,
	fileID integer not null,
	featureID integer not null,
	foreign key(fileID) references file(id),
	foreign key(featureID) references feature(id)
);