create table feature (
	id integer primary key not null,
	projectID text not null,
	name text not null,
	finished varchar(1) not null,
	tested varchar(1) not null,
	foreign key(projectID) references project(id)
);