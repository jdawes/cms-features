create table session (
	id integer primary key not null,
	projectID integer not null,
	current integer(1) not null,
	foreign key(projectID) references project(id)
);