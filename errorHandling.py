# Error handling class
# Will need to work on this to allow better debugging of feature annotation code

import sys

class errorHandling():
	fileName = None
	queueList = None
	def __init__(self, fileName):
		# fileName is the file that is being parsed,
		# hence the file that the error will occur in
		self.fileName = fileName
		self.queueList = []

	def throwWithLine(self, message, lineNumber, exitOnReport=True):
		print("An error occurred while parsing statement beginning at line %s:\n\t" % lineNumber)
		print(message)
		print("------------------------------------\n")
		if exitOnReport:
			exit()

	def throw(self, message, exitOnReport=True):
		print("An error occurred while parsing:\n\t")
		print(str(message))
		print("------------------------------------\n")
		if exitOnReport:
			exit()

	def throwApplicationError(self, message, exitOnReport=True):
		# extra new line in case we're handling keyboard interrupt
		print("\n" + str(message) + "\n")
		if exitOnReport:
			exit()

	# this function is for if we're inside a function and don't want to throw the error yet
	# we can use processQueue to process all errors in the queue
	def queue(self, message, fileName=None):
		if fileName == None:
			self.queueList.append(str(message))
		else:
			self.queueList.append(("File '%s':\n" + message) % fileName)

	def processQueue(self, lineNumber):
		originalLength = len(self.queueList)
		while len(self.queueList) > 0:
			self.throwWithLine(self.queueList.pop(), lineNumber, exitOnReport=False)
		# only exit if there were actually errors
		if originalLength > 0:
			sys.exit(0)