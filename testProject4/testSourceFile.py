#fragment:begin(name="feature 1",
#				ref="#1.1",
#				finished=no,
#				tested=no,
#				comments="Some comment about the code in this code fragment")
#fragment:begin(name="feature 3", ref="#2", finished=no, tested=no)


print("some information")

print("this code contributes to feature 1")

#fragment:end

print("some extra code here")
print("This separates the features a bit")

#fragment:end

#fragment:begin(name="feature 2", ref="#3", finished=yes, tested=no)

print("some code that does feature 2")

#fragment:end

#fragment:begin(name="feature 3", ref="#4", finished=yes, tested=yes)
#fragment:begin(name="feature 2", ref="#5", finished=no, tested=no)

for n in range(0, 10):
	print(n)

subTotal = 0
for n in range(0, 100):
	subTotal += n**2

#fragment:end
#fragment:end

#fragment:begin(name="order things", ref="#6", finished=no, tested=yes)

things = [324, 234, 123, 654, 123, 456, 123, 564]
things = sorting(things)
# the list is now sorted, thus fulfilling the requirement "order things".

print('some more code here that does something else that contributes to this requirement")

#fragment:end

#fragment:begin(name="make sure a number is even", ref="#7", finished=yes, tested=yes,
#			comments="We need to check that the number given is even")
numberIsEven = number % 2 == 0
if numberIsEven:
	print("the number is even")
else:
	print("the number is not even")
#fragment:end
