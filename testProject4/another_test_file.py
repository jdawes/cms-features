#fragment:begin(name="accept input", finished="no", tested="no",
#	       ref="input block",
#	       comments="This block accepts input from the user.")

input = raw_input("Ask for some input")

#fragment:end
#fragment:begin(name="make sure a number is even", ref="even number function", finished="no", tested="yes")

def numberIsEven(n):
	return n % 2 == 0

#fragment:end
