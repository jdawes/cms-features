from sqlalchemy import Column, Integer, String, DateTime, Sequence, ForeignKey
from sqlalchemy.orm import relationship

# ORM classes must extend Base to inherit necessary properties
# to be associated with a database tale
from base import Base

valueToInteger = {"yes" : 1, "no" : 0}

class Project(Base):
	__tablename__ = "project"
	id = Column(Integer(), Sequence("project_id"), primary_key=True)
	name = Column(String(), nullable=False)
	directory = Column(String(), nullable=False)
	current = Column(Integer(), nullable=False)
	features = relationship("Feature", backref="project", cascade="all, delete, delete-orphan")
	files = relationship("File", backref="project", cascade="all, delete, delete-orphan")

	def __init__(self, dictionary):
		self.name = dictionary["name"]
		self.directory = dictionary["directory"]
		self.current = dictionary["current"]

	def __repr__(self):
		return "<project (id=%s, name=\"%s\", directory=\"%s\")>" % (self.id, self.name, self.directory)

class Feature(Base):
	__tablename__ = "feature"
	id = Column(Integer(), Sequence("feature_id"), primary_key=True)
	projectID = Column(Integer(), ForeignKey("project.id"), nullable=False)
	name = Column(String(), nullable=False)
	finished = Column(Integer())
	tested = Column(Integer())
	# initialise a foreign key relationship
	# this instance variable will hold a set of codeFragment records
	# with linked to this object by primary key
	codeFragments = relationship("CodeFragment", backref="feature", cascade="all, delete, delete-orphan")

	def __init__(self, dictionary):
		self.projectID = dictionary["projectID"]
		self.name = dictionary["name"]
		self.finished = valueToInteger[dictionary["finished"]]
		self.tested = valueToInteger[dictionary["tested"]]

	def __repr__(self):
		return "<feature (projectID=\"%s\", name=\"%s\", finished=%s, tested=%s)>" %\
				(self.projectID, self.name, self.finished, self.tested)

class CodeFragment(Base):
	__tablename__ = "codeFragment"
	id = Column(Integer(), Sequence("code_fragment_id"), primary_key=True)
	finished = Column(Integer())
	tested = Column(Integer())
	comments = Column(String(), nullable=False)
	startLine = Column(Integer())
	endLine = Column(Integer())
	ref = Column(String())

	# these are both foreign keys, so add a foreign key reference
	fileID = Column(Integer(), ForeignKey("file.id"))
	featureID = Column(Integer(), ForeignKey("feature.id"))

	def __init__(self, dictionary):
		# temporarily take all values exactly
		# later on, we won't be able to do this since fileID and featureID will
		# have to be fetched by a query
		try:
			self.finished = valueToInteger[dictionary["finished"]]
		except KeyError:
			self.finished = 0
		try:
			self.tested = valueToInteger[dictionary["tested"]]
		except KeyError:
			self.tested = 0
		try:
			self.comments = dictionary["comments"]
		except KeyError:
			self.comments = ""
		self.ref = dictionary["ref"]
		self.startLine = dictionary["startLine"]
		self.endLine = dictionary["endLine"]
		self.fileID = dictionary["fileID"]

	def __repr__(self):
		return "<codeFragment (id=%s, finished=%s, tested=%s, comments=\"%s\", startLine=%s, endLine=%s, fileID=%s, featureID=%s)>" %\
				(self.id, self.finished, self.tested, self.comments, self.startLine, self.endLine, self.fileID, self.featureID)

class File(Base):
	__tablename__ = "file"
	id = Column(Integer(), Sequence("file_id"), primary_key=True)
	fileName = Column(String(), nullable=False)
	numberOfLines = Column(Integer(), nullable=False)
	projectID = Column(String(), ForeignKey("project.id"), nullable=False)
	hash = Column(String(), nullable=False)
	codeFragments = relationship("CodeFragment", backref="file", cascade="all, delete, delete-orphan")

	def __init__(self, dictionary):
		self.fileName = dictionary["fileName"]
		self.projectID = dictionary["projectID"]
		self.numberOfLines = dictionary["numberOfLines"]
		self.hash = dictionary["hash"]

	def __repr__(self):
		return "<file (id=%s, fileName=\"%s\", projectID=\"%s\")>" % (self.id, self.fileName, self.projectID)

class Session(Base):
	__tablename__ = "session"
	id = Column(Integer(), Sequence("session_id"), primary_key=True)
	projectID = Column(Integer(), nullable = False)
	current = Column(Integer(), nullable=False)

	def __init__(self, dictionary):
		self.projectID = dictionary["projectID"]
		self.current = dictionary["current"]

	def __repr__(self):
		return "<session (id=%s, projectID=%s, current=%s)>" %\
				(self.id, self.projectID, self.current)

"""# test ORM
if __name__ == "__main__":
	from sqlalchemy.orm import sessionmaker
	from sqlalchemy import create_engine
	engine = create_engine("sqlite:///tracking.db")
	session = sessionmaker(bind=engine)()

	print(session.query(CodeFragment).all())"""